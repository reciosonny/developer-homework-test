import { GetCostPerBaseUnit } from "../supporting-files/helpers";
import {
  NutrientFact,
  Product,
  SupplierProduct,
} from "../supporting-files/models";

/**
 * Functionality: Gets the cheapest product by getting an array of products regardless of the supplier
 * @param products: List of products to compare the cheapest product we can get against
 * @returns Product
 */
export const getCheapestProduct = (products: Product[]) => {
  const mappedProducts = products
    .map((p) =>
      p.supplierProducts.map((sProducts) => ({
        ...sProducts,
        nutrientFacts: p.nutrientFacts,
      }))
    )
    .reduce((init, curr) => [...init, ...curr], []); //flat reduce

  const cheapestProduct = mappedProducts.reduce(
    (init: any | null, curr: any) => {
      if (init) {
        if (curr.supplierPrice < init.supplierPrice) {
          return curr;
        }
      } else {
        //initialize null value
        return curr;
      }

      return init;
    },
    null
  );

  const mappedCheapestProductCost = { ...cheapestProduct, cheapestCost: GetCostPerBaseUnit(cheapestProduct as SupplierProduct) };

  return mappedCheapestProductCost;
};

/**
 * Functionality: Utility library to merge/sum the nutrition facts together
 * @param currentNutrient 
 * @param newNutrientInfoToMerge 
 * @returns 
 */
export const mergeNutritionUomAmount = (currentNutrient: NutrientFact, newNutrientInfoToMerge: NutrientFact) => {


  const result = {
    ...currentNutrient,
    quantityAmount: {
      ...currentNutrient.quantityAmount,
      uomAmount:
        currentNutrient.quantityAmount.uomAmount +
        newNutrientInfoToMerge.quantityAmount.uomAmount,
    },
  };

  return result;
};
