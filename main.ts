import {
    GetRecipes
} from "./supporting-files/data-access";
import {RunTest, ExpectedRecipeSummary} from "./supporting-files/testing";
import * as productService from "./services/productService";

console.clear();
console.log("Expected Result Is:");
console.dir(ExpectedRecipeSummary, { depth: null });

const recipeData = GetRecipes(); // the list of 1 recipe you should calculate the information for

/*
 * YOUR CODE GOES BELOW THIS, DO NOT MODIFY ABOVE
 * (You can add more imports if needed)
 * */

//Functionality: We take an array of recipeData and summarize it using reduce to loop through the list of recipes. We used getRecipeSummary service call to summarize the recipe and reduce it to present cheapest ingredients at its cheapest cost
const recipeSummary: any = recipeData.reduce((initial, recipe) => {
    const result = productService.getRecipeSummary(recipe);

    initial[recipe.recipeName] = result;

    return initial;
}, {});

/*
 * YOUR CODE ABOVE THIS, DO NOT MODIFY BELOW
 * */
RunTest(recipeSummary);
