import { GetProductsForIngredient } from "../supporting-files/data-access";
import {
  GetCostPerBaseUnit,
  GetNutrientFactInBaseUnits,
} from "../supporting-files/helpers";
import {
  NutrientFact,
  Recipe,
} from "../supporting-files/models";
import * as productUtils from "../utils/productUtils";

/**
 * Functionality: Gets the recipe summary we need to get the following information:
 * - Cheapest ingredients/products
 * - Compute cheapest cost from products
 * - Compute the sum of cheapest nutritional facts from ingredients
 * @param recipe Recipe information
 * @returns Object
 */
export const getRecipeSummary = (recipe: Recipe): any => {
  const ingredientProducts = recipe.lineItems.map((lineItem) => ({
    ...lineItem,
    cheapestProduct: productUtils.getCheapestProduct(
      GetProductsForIngredient(lineItem.ingredient)
    ),
  }));

  const recipeSummary = ingredientProducts.reduce(
    (current, ingredientProduct) => {
      const product = ingredientProduct.cheapestProduct;      
      const cheapestCost = GetCostPerBaseUnit(product);

      current.cheapestCost += cheapestCost; //sums the cheapest cost based on its base unit

      product.nutrientFacts.forEach((n: NutrientFact) => {
        const nutrient = GetNutrientFactInBaseUnits(n);

        const { nutrientName } = nutrient;
        let currentNutrient = current.nutrientsAtCheapestCost[nutrientName];

        if (currentNutrient) {
          current.nutrientsAtCheapestCost[nutrientName] = productUtils.mergeNutritionUomAmount(currentNutrient, nutrient);
        } else { //initialize nutrients
          current.nutrientsAtCheapestCost[nutrientName] = { ...nutrient };
        }
      });

      return current;
    },
    { cheapestCost: 0, nutrientsAtCheapestCost: {} }
  );

  return recipeSummary;
};
